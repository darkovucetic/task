function appendhtml(spanText){
    return "<div class=\"checkBoxDiv\">\n" +
        "   <label class=\"checkBox\">\n" +
        "       <input type=\"checkbox\" checked=\"checked\">\n" +
        "       <span class=\"checkmark\"></span>\n" +
        "       <span class=\"checkmarkText\">"+spanText+"</span>\n" +
        "       <span class=\"price\">5,51&euro;</span>\n" +
        "   </label>\n" +
        "</div>";
}
var productMod = (function(){
    var notSelectedDiv = $(".notSelectedProductsArea");
    var selectedProduct = $(".blueProducts .productArea");
    var bonusProduct = $(".greenProducts .productArea");
    var sortable = $(".sortableArea");
    var gift = $(".gift");
    var switchToogle =$(".switch input");

    sortable.sortable({
        items : '.product:not(.gift)',
        connectWith: sortable,
        opacity:0.5,
        distance: 5,
        revert: 300,
        tolerance: "pointer",
        cursor: "move",
        update:function (event, ui) {
            var text;
            if (this === ui.item.parent()[0]) {
                if($(ui.item).find("span.fa-times").length ===0 && $(this).hasClass("productArea")){
                    text = $(ui.item).find("span.actionProduct")
                        .removeClass("fa-plus")
                        .addClass("fa-times")
                        .siblings("p")
                        .text();
                    var html = appendhtml(text);
                    $(".checkBoxProduct").append(html);
                    if(notSelectedDiv.find(".product:not(.gift)").length === 0){
                        gift.appendTo(bonusProduct);
                        switchToogle.prop("checked",true);
                    }
                }
                else if($(this).hasClass("notSelectedProductsArea")){
                    text =$(ui.item).find("span.actionProduct")
                        .removeClass("fa-times")
                        .addClass("fa-plus")
                        .siblings("p")
                        .text();
                    $(".checkBox span.checkmarkText:contains("+text+")").parentsUntil(".checkBoxProduct").remove();
                    if(notSelectedDiv.find(".product:not(.gift)").length >=1){
                        gift.appendTo(notSelectedDiv);
                        switchToogle.prop("checked",false)
                    }
                }
            }
        }
    });
    var addProduct = function () {
        var product = $(this);
        product.removeClass("fa-plus")
            .addClass("fa-times")
            .parent("div")
            .appendTo(selectedProduct);

        if(notSelectedDiv.find(".product:not(.gift)").length === 0){
            gift.appendTo(bonusProduct);
            switchToogle.prop("checked",true);
        }
        var html = appendhtml(product.siblings("p").text());

        $(".checkBoxProduct").append(html);
    };

    var removeProduct = function () {
        var product = $(this);
        var text = product.siblings("p").text();
        product.removeClass("fa-times")
            .addClass("fa-plus")
            .parent("div")
            .appendTo(notSelectedDiv);
        if(notSelectedDiv.find(".product:not(.gift)").length >= 1){
            gift.appendTo(notSelectedDiv);
            switchToogle.prop("checked",false)
        }
        $(".checkBox span.checkmarkText:contains("+text+")").parentsUntil(".checkBoxProduct").remove();
    };

    var moveAll = function () {
        if(notSelectedDiv.find(".product").length >= 1) {

            notSelectedDiv.find(".product").each(function () {
                $(".checkBoxProduct").append(appendhtml($(this).find("p").text()));
            });
            notSelectedDiv.find(".gift").appendTo(bonusProduct);
            notSelectedDiv.find(".product").appendTo(selectedProduct)
                .find("span.actionProduct")
                .removeClass("fa-plus")
                .addClass("fa-times");
        }
        else{
            selectedProduct.find(".product").appendTo(notSelectedDiv)
                .find("span.actionProduct")
                .removeClass("fa-times")
                .addClass("fa-plus");
            gift.appendTo(notSelectedDiv);
            $(".checkBoxProduct").html('');
        }
    };

    var eventBinder = function () {
        notSelectedDiv.on("click",".actionProduct", addProduct);
        selectedProduct.on("click",".actionProduct", removeProduct);
        switchToogle.on("click", moveAll);
    };
    return{
        eventBinder : eventBinder
    }
})();



$(document).ready(function () {
    productMod.eventBinder();
});